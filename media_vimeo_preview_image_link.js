(function($) {

  Drupal.behaviors.mediaVimeoPreviewImageLink = {
    attach: function(context, settings) {

      // Process links that should show video in an IFRAME.
      $('.media-vimeo-preview-image-link-iframe', context).once().click(function() {

        // Initialize some values.
        var $link = $(this);
        var settingID = $link.attr('iframe-setting-id');
        if (!settingID) return;
        var $image = $link.find('img:first');
        if (!$image.size()) return;
        var width = $image.width();
        var height = $image.height();
        if (!width || !height) return;
        var iframeData = $link.attr('iframe-data');
        if (!iframeData) return;
        iframeData = decodeURIComponent(iframeData);

        // We used placeholders for weight and height. Set them now from IMG.
        iframeData = iframeData.replace(/--iframe-width--/g, width);
        iframeData = iframeData.replace(/--iframe-height--/g, height);
        // Also replace placeholders in settings.
        Drupal.settings.media_vimeo[settingID].width = width;
        Drupal.settings.media_vimeo[settingID].height = height;

        // Finally replace the image with video.
        var $wrapper = $('<div></div>').insertAfter($link);
        $wrapper.html(iframeData);
        $link.replaceWith($wrapper);
        Drupal.behaviors.media_vimeo.attach($wrapper, settings);
        return false;
      });
    }
  };

})(jQuery);